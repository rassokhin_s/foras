jQuery(document).ready(function($) {
    $('#homepageSlider').slick({
        infinite: true,
        speed: 900,
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000
    });
});
