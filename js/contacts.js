jQuery(document).ready(function($) {
    $('#navList').find('.nav__link[data-link="contacts"]').addClass('active');

    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init(){
        myMap = new ymaps.Map("map", {
            center: [47.200954, 38.940404],
            zoom: 18,
            controls: ['smallMapDefaultSet']
        });

        myPlacemark = new ymaps.Placemark([47.200954, 38.940404], {
            hintContent: 'ООО "Форас"',
            balloonContent: 'пер. Гарибальди, 78'
        });

        myMap.geoObjects.add(myPlacemark);
    }
});