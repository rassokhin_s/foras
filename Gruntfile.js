module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        //------------------------------------------------------------
        less: { // Task less
            options: {
                expand: true
            },
            dev: { // Target
                options: {
//                    strictMath: true,
                    sourceMap : true,
                    sourceMapFilename: 'css/styles.css.map',
                    sourceMapRootpath: '../'
                },
                files: {
                    'css/styles.css': ['less/all.less']
                }
            },
            release: { // Target
                options: {
//                    strictMath: true,
                    yuicompress: true,
                    sourceMap : true,
                    sourceMapFilename: 'css/styles.css.map',
                    sourceMapRootpath: '../'
                },
                files: {
                    'css/styles.css': ['less/all.less']
                }
            }
        },
        jade: {
            compile: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [ {
                    src: "*.jade",
                    dest: "./",
                    expand: true,
                    ext: ".html"
                } ]
            }
        },
        clean: ["*.html"],
        watch: {
            less: {
                files: 'less/**',
                tasks: ['less:dev'],
                options: {
                    interrupt: true
                }
            },
            css: {
                files: ['*.css']
            },
            jade: {
                files: ['*.jade', 'jade_includes/*.jade'],
                tasks: ['clean', 'jade:compile']
            },
            livereload: {
                options: { livereload: true },
                files: ['css/*.css']
            }
        }
        //------------------------------------------------------------
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-clean');
};